import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { type Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import ProductService from '@/services/product'

export const usePosStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()
  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])
  async function getProducts() {
    try {
      loadingStore.doLoad()
      let res = await ProductService.getProductsByType(1)
      Products1.value = res.data
      res = await ProductService.getProductsByType(2)
      Products2.value = res.data
      res = await ProductService.getProductsByType(3)
      Products3.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('Error')
      loadingStore.finish()
    }
  }
  return { products1, products2, products3, getProducts }
})
