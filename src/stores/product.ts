import { useLoadingStore } from './loading'
import { useMessageStore } from './message';
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import ProductService from '@/services/Product'
import type { Product } from '@/types/Product'

export const useProductStore = defineStore('Product', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const Products = ref<Product[]>([])
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    type: { id: 2, name: 'drink' },
    image: 'noimage.jpg',
    files: []
  }
  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))

  async function getProduct(id: number) {
    try {
      loadingStore.doLoad()
      const res = await ProductService.getProduct(id)
      editedProduct.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
    loadingStore.doLoad()
    const res = await ProductService.getProduct(id)
    editedProduct.value = res.data
    loadingStore.finish()
  }
  async function getProducts() {
    try {
      loadingStore.doLoad()
      const res = await ProductService.getProducts()
      Products.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('Error')
      loadingStore.finish()
    }
  }
  async function saveProduct() {
    try {
      loadingStore.doLoad()
      const Product = editedProduct.value
      if (!Product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(Product))
        const res = await ProductService.addProduct(Product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(Product))
        const res = await ProductService.updateProduct(Product)
      }
  
      await getProducts()
      loadingStore.finish()
    }  
    } catch (e: any) {
      messageStore.showMessage(e.messsage)
      loadingStore.finish()
    }

  async function deleteProduct() {
    loadingStore.doLoad()
    const Product = editedProduct.value
    const res = await ProductService.delProduct(Product)

    await getProducts()
    loadingStore.finish()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }
  return { Products, getProducts, saveProduct, deleteProduct, editedProduct, getProduct, clearForm }
})
